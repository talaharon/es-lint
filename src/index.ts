import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

const response = saySomething('hello');
log.magenta(response);

log.green('Changes for branch two');
log.red('Changes for branch one');
log.v('Changes');